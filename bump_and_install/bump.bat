:: Bump the version
bumpversion --config-file .bumpversion.cfg minor --allow-dirty

:: Pause to let user verify
pause