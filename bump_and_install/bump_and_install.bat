:: Move to folder with setup.py and bumpversion config-file
cd ..

:: Bump the version
bumpversion --config-file .bumpversion.cfg minor --allow-dirty

:: Build the current version
C:\Python37\python.exe setup.py sdist bdist_wheel

:: Get the most recently created version
for /f %%i in ('dir dist /b/a-d/od/t:c') do set LAST=%%i

:: Install that version for Python 3
C:\Python37\Scripts\pip.exe install --force-reinstall "dist\\%LAST%"

:: Pause to let user verify
pause
