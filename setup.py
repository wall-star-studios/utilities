"""Package settings for WS Utilities."""
import setuptools

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="ws-utilities",
    version="0.3.0",
    scripts=[],
    author="Jason Swanson",
    author_email="jasonthecpe@gmail.com",
    description="Utilities for Wall Star Studios",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wall-star-studios/utilities",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Unlicensed",
        "Operating System :: OS Independent",
    ],
)
