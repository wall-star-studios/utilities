"""Tests for security.py from ws_utilities."""

import unittest
from hypothesis.strategies import integers, text
from hypothesis import given, settings
from ws_utilities import security


class TestSecurity(unittest.TestCase):
    """
    Test cases for the security.py from ws_utilities.

    Utilizes hypothesis for more robust testing on most methods.
    """

    @settings(max_examples=200)
    @given(
        hash_size=integers(max_value=2048),
        hexdigest_size=integers(max_value=256)
    )
    def test_calculate_dklen_for_hash_size(self, hash_size, hexdigest_size):
        """
        Tests calculate_dklen_for_hash_size.
        Makes sure it returns the correct sizes and throws errors where
        expected.

        Args:
            hash_size (bool): [description]
            hexdigest_size ([type]): [description]
        """
        if hash_size < 2:
            self.assertRaisesRegex(
                ValueError,
                r"hash_size \(int=[-0-9]+\) must be 2 or more",
                security.calculate_dklen_for_hash_size,
                hash_size, hexdigest_size
            )
        elif hexdigest_size < 0:
            self.assertRaisesRegex(
                ValueError,
                r"hexdigest_size \(int=[-0-9]+\) must be 0 or more",
                security.calculate_dklen_for_hash_size,
                hash_size, hexdigest_size
            )
        elif hash_size < hexdigest_size + 2:
            self.assertRaisesRegex(
                ValueError,
                r"hash_size \(int=[-0-9]+\) must be at least 2 larger than hexdigest_size \(int=[0-9]+\)",
                security.calculate_dklen_for_hash_size,
                hash_size, hexdigest_size
            )
        else:
            self.assertEqual(
                security.calculate_dklen_for_hash_size(
                    hash_size, hexdigest_size
                ),
                (hash_size - hexdigest_size + 1) // 2
            )

    @settings(max_examples=200)
    @given(
        token_size=integers(max_value=2048),
        salt_size=integers(max_value=512)
    )
    def test_generate_token(self, salt_size, token_size):
        """
        Tests generate_token.
        Makes sure it is returning tokens of correct size and throwing errors
        where expected.

        Args:
            salt_size ([int]): Salt size to be tested.
            token_size ([int]): Token size to be tested.
        """
        if token_size < 2:
            self.assertRaisesRegex(
                ValueError,
                r"token_size \(int=[-0-9]+\) must be at least 2",
                security.generate_token,
                token_size, salt_size
            )
        elif salt_size < 0:
            self.assertRaisesRegex(
                ValueError,
                r"salt_size \(int=[-0-9]+\) must be at least 0",
                security.generate_token,
                token_size, salt_size
            )
        else:
            self.assertEqual(
                len(security.generate_token(token_size, salt_size)),
                token_size
            )

    @settings(max_examples=200)
    @given(
        password=text(min_size=0, max_size=128),
        hash_size=integers(min_value=8, max_value=2048),
        salt_size=integers(min_value=0, max_value=512)
    )
    def test_hash_and_verify_password(self, password, salt_size, hash_size):
        """
        Tests hash_password, hash_password_rand_salt, and verify_password.
        Makes sure they work and work together and throw errors where expected.

        Args:
            password (str): Password to be tested.
            salt_size (int): Salt size to be tested.
            hash_size (int): Hash size to be tested.
        """
        if len(password) < 1:
            self.assertRaisesRegex(
                ValueError,
                r"Please provide a password",
                security.hash_password_rand_salt,
                password, hash_size, salt_size, iterations=2
            )
        elif hash_size < security.HEXDIGEST_SIZE + 2:
            self.assertRaisesRegex(
                ValueError,
                r"hash_size \(int=[-0-9]+\) must be at least 2 larger than hexdigest_size \(int=[-0-9]+\)",
                security.hash_password_rand_salt,
                password, hash_size, salt_size, iterations=2
            )
        else:
            hash = security.hash_password_rand_salt(
                password, hash_size, salt_size, iterations=2)
            self.assertEqual(len(hash), hash_size)
            self.assertTrue(security.verify_password(
                hash, password, iterations=2))
            self.assertFalse(security.verify_password(
                hash, password[1:], iterations=2))

    def test_verify_password_requirements(self):
        """
        Tests that verify_password_requirements works for the default
        requirement configuration.
        """
        self.assertFalse(security.verify_password_requirements(""))
        self.assertFalse(security.verify_password_requirements("AAAAAAAA"))
        self.assertFalse(security.verify_password_requirements("Aaaaaaaa"))
        self.assertFalse(security.verify_password_requirements("12345678"))
        self.assertFalse(security.verify_password_requirements("123Ab"))
        self.assertFalse(security.verify_password_requirements("AzA3k"))
        self.assertTrue(security.verify_password_requirements("123ABcde"))
        self.assertTrue(security.verify_password_requirements("sfdJ3Ksk"))
        self.assertTrue(security.verify_password_requirements("vty7uijKL"))


if __name__ == "__main__":
    unittest.main()
