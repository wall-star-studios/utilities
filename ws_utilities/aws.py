"""Utilities for interfacing with AWS services."""

import json
import boto3


def get_secret(region_name: str, secret_id: str) -> dict:
    """
    Retrieve secret info from Secret Manager.

    Args:
        region_name(str): AWS region name
        secret_id(str): Secret Manager secret id to read from

    Returns:
        A dict containing the secret contents.

    """
    client = boto3.session.Session().client(
        service_name='secretsmanager',
        region_name=region_name
    )
    resp = client.get_secret_value(SecretId=secret_id)
    return json.loads(resp['SecretString'])
