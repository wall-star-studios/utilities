"""Utilities for interfacing with psycopg2."""

import contextlib
import logging
from typing import Any
from psycopg2.extras import RealDictCursor
from . import aws


def get_secret_rds_config(secret_id: str, region: str) -> dict:
    """
    Get psycopg2 compatable configurations from AWS SecretManager.

    Args:
        secret_id (str): the AWS Secret id
        region (str): the region for the secret

    Returns:
        dict: the secret contents

    """
    secret = aws.get_secret(region, secret_id)
    return {
        'host': secret['host'],
        'port': secret['port'],
        'user': secret['username'],
        'password': secret['password']
    }


@contextlib.contextmanager
def get_conn(pool):
    """
    Get a connection from the connection pool.

    Must be called within a "with" context statement.

    Args:
        pool: the connection pool to pull from

    Returns:
        A connection from the connection pool.

    """
    # Makes the connection pool work with the "with" statement.
    # https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager
    conn = pool.getconn()
    try:
        yield conn
    finally:
        pool.putconn(conn)


# See following for implementation source.
# http://initd.org/psycopg/docs/advanced.html#connection-and-cursor-factories

# Logging cursors and dictionary cursors already exist in psycopg2.
# * http://initd.org/psycopg/docs/extras.html#logging-cursor
# * http://initd.org/psycopg/docs/extras.html#real-dictionary-cursor
# But it doesn't seem possible to combine them without defining a new class.

# Also the LoggingCursor is coupled with the LoggingConnection class.
# https://github.com/psycopg/psycopg2/blob/be8e1a26326edb6b094a764797460181ae8b33b6/lib/extras.py#L456

class LoggingDictCursor(RealDictCursor):
    """Dictionary cursor which also logs the queries it executes."""

    logger = logging.getLogger('LoggingDictCursor')

    # pylint complains about this no matter what we do because the
    # RealDictCursor super class decided to use vars here
    def execute(self, query: str, vars: Any = None):
        """
        Log and execute the query.

        Args:
            query (str): the query to execute
            vars (Any, optional): the variables to use with the query. Defaults
                to None.

        Returns:
            Any: the query results

        """
        try:
            LoggingDictCursor.logger.info(
                "Executing query: %s", self.mogrify(query, vars).decode())
            return super(LoggingDictCursor, self).execute(query, vars)
        except Exception as exc:
            LoggingDictCursor.logger.error(
                "%s: %s", exc.__class__.__name__, exc)
            raise
