"""
Module with utilities for handling passwords securely.

Also functions for generating authentication tokens.
"""

import hashlib
import binascii
import os
import re
import datetime
from typing import Tuple

# size of the data returned by hexdigest when called on our salt values
HEXDIGEST_SIZE = 64
# default size of tokens
DEFAULT_TOKEN_SIZE = 32
# default size of the password hash
DEFAULT_HASH_SIZE = 256
# default size of the salt (before hexdigest)
DEFAULT_SALT_SIZE = 60
# default number of iterations when hashing
DEFAULT_ITERATIONS = 100000

# regexes for verifying password meets our security standards
DEFAULT_PASS_REQUIREMENTS = (
    re.compile(r".*[a-z]"),                     # must have a lowercase letter
    re.compile(r".*[A-Z]"),                     # must have a uppercase letter
    re.compile(r".*[0-9]"),                     # must have a number
    re.compile(r".{8,}")                        # must be at least 8 characters
)

# regex for special character requirement (disabled)
# re.compile(r".*[!#$%&'\"()*+,-.:;<=>?@]")  # must have a special character


def calculate_dklen_for_hash_size(hash_size: int,
                                  hexdigest_size: int = HEXDIGEST_SIZE) -> int:
    """
    Calculate the size of the dklen variable for a hash output of given size.
    Rounds up for odd hash sizes.

    Args:
        hash_size (int): the output hash size
        hexdigest_size (int, optional): the size of the hexdigest generated
        from the salt that is added to the hash and factored into the final
        size. Defaults to HEXDIGEST_SIZE.

    Returns:
        int: The size of dklen for a particular hash output size.

    Raises:
        ValueError:
            - if input hash_size is less than 2
            - if hexdigest_size is less than 0
            - input hash is smaller than the hexdigest_size
    """
    if hash_size < 2:
        raise ValueError(f"hash_size (int={hash_size}) must be 2 or more")
    if hexdigest_size < 0:
        raise ValueError(
            f"hexdigest_size (int={hexdigest_size}) must be 0 or more")
    if hash_size < hexdigest_size + 2:
        raise ValueError(
            f"hash_size (int={hash_size}) must be at least 2 larger than hexdigest_size (int={hexdigest_size})")

    return (hash_size - hexdigest_size + 1) // 2


def generate_token(token_size: int = DEFAULT_TOKEN_SIZE,
                   salt_size: int = DEFAULT_SALT_SIZE) -> str:
    """
    Generate a token by hashing random salt and the current time.

    Args:
        token_size (int, optional): Size of the token.
            Defaults to DEFAULT_TOKEN_SIZE.
        salt_size (int, optional): Size of the random salt.
            Defaults to DEFAULT_SALT_SIZE.

    Returns:
        str: A token that can be used for authentication by certain API calls.

    Raises:
        ValueError:
            - if token_size is less than 2
    """
    if token_size < 2:
        raise ValueError(f"token_size (int={token_size}) must be at least 2")
    if salt_size < 0:
        raise ValueError(f"salt_size (int={salt_size}) must be at least 0")

    salt = hashlib.sha256(os.urandom(salt_size))
    salt = salt.hexdigest().encode('ascii')
    timestamp = str(datetime.datetime.now())
    token = hashlib.pbkdf2_hmac('sha512',
                                timestamp.encode('utf-8'),
                                salt, 10,
                                calculate_dklen_for_hash_size(
                                    token_size, 0)
                                )
    token = binascii.hexlify(token).decode('ascii')

    if token_size < len(token):  # to match when we have odd token_size
        return token[:-1]
    return token


def hash_password(password: str, salt: str, hash_size: int = DEFAULT_HASH_SIZE,
                  iterations: int = DEFAULT_ITERATIONS,
                  prefix_salt: bool = True,) -> str:
    """
    Hash a password with provided salt.
    The last character is truncated in the case of odd length hashes.
    Note that the smaller a password hash is the the more easily it can be
    cracked and so a larger hash size is highly recommended.

    Args:
        password (str): The password to be hashed.
        salt (str): The salt to use when hashing the password.
        hash_size (int, optional): Length of the final hash output.
            Defaults to DEFAULT_HASH_SIZE.
        iterations (int, optional): How many iterations of the hashing function
            to run through. Defaults to DEFAULT_ITERATIONS.
        prefix_salt (bool, optional): Whether we want to include the salt as a
            prefix on the returned hash. Defaults to True.

    Returns:
        str: The hashed and salted password.
    """
    if len(password) < 1:
        raise ValueError("Please provide a password")

    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  password.encode('utf-8'),
                                  salt, iterations,
                                  calculate_dklen_for_hash_size(hash_size))
    pwdhash = binascii.hexlify(pwdhash)

    if prefix_salt:
        pwdhash = (salt + pwdhash).decode('ascii')
    else:
        pwdhash = pwdhash.decode('ascii')

    if len(pwdhash) > hash_size:  # rounded up with dklen due to odd hash size
        pwdhash = pwdhash[:-1]

    return pwdhash


def hash_password_rand_salt(password: str, hash_size: int = DEFAULT_HASH_SIZE,
                            salt_size: int = DEFAULT_SALT_SIZE,
                            iterations: int = DEFAULT_ITERATIONS) -> str:
    """
    Hash a password for storing with random salt of a given length.

    Args:
        password (str): The password to be hashed.
        hash_size (int, optional): Length of the final hash output.
            Defaults to DEFAULT_HASH_SIZE.
        salt_size (int, optional): Size of the random salt.
            Defaults to DEFAULT_SALT_SIZE.
        iterations (int, optional): How many iterations of the hashing function
            to run through. Defaults to DEFAULT_ITERATIONS.

    Returns:
        str: The hashed and salted password.
    """
    salt = hashlib.sha256(os.urandom(salt_size)).hexdigest().encode('ascii')
    return hash_password(password, salt, hash_size, iterations)


def verify_password(stored_password_hash: str, provided_password: str,
                    iterations: int = DEFAULT_ITERATIONS) -> bool:
    """
    Verify a stored password against one provided by user.

    Args:
        stored_password_hash (str): The salted and hashed password.
        provided_password (str): The password we are checking against the
            stored password hash.
        iterations (int, optional): How many iterations of the hashing function
            to run through. Defaults to DEFAULT_ITERATIONS.

    Returns:
        bool: True if the hashed provided password matches the
        stored_password_hash, false otherwise.
    """
    if len(provided_password) < 1:
        return False

    salt = stored_password_hash[:HEXDIGEST_SIZE].encode('ascii')
    pwdhash = hash_password(provided_password, salt,
                            len(stored_password_hash),
                            iterations)
    return pwdhash == stored_password_hash


def verify_password_requirements(password: str,
       requirements: Tuple[re.Pattern] = DEFAULT_PASS_REQUIREMENTS) -> bool:
    """
    Check that a password meets our security requirements.

    Args:
        password (str): The password being checked.
        requirements (Tuple[re.Pattern]): collection of compiled regex patterns
            that must all be matched for a password to be considered valid.

    Returns:
        bool: True if the password meets our security requirements, False
        otherwise.
    """
    for regex in requirements:
        if not regex.match(password):
            return False
    return True
